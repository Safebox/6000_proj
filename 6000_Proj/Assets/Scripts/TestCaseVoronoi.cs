﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCaseVoronoi : MonoBehaviour
{
    public int numberOfSites;
    public int voronoiSize;
    public int seed;
    private float[,,] splatmapData;
    public bool applySplatmap;

    void Start()
    {
        if (PlayerPrefs.GetInt("terrainGen") == 1)
        {
            seed = PlayerPrefs.GetInt("terrainSeed");

            //Resize terrain as necessary
            this.GetComponent<Terrain>().terrainData.heightmapResolution = voronoiSize + 1;
            this.GetComponent<Terrain>().terrainData.size = new Vector3(500, 255, 500);
            this.GetComponent<Terrain>().terrainData.alphamapResolution = voronoiSize;

            //Setup Voronoi diagram
            Voronoi v = new Voronoi(voronoiSize, voronoiSize);
            v.PlaceSitesAtRandom(numberOfSites, seed);
            long vStartTime = DateTime.UtcNow.Ticks;
            v.GenerateEuclidean();
            long vEndTime = (DateTime.UtcNow.Ticks - vStartTime);

            //Setup splatmaps
            long sStartTime = DateTime.UtcNow.Ticks;
            splatmapData = new float[this.GetComponent<Terrain>().terrainData.alphamapWidth, this.GetComponent<Terrain>().terrainData.alphamapHeight, this.GetComponent<Terrain>().terrainData.alphamapLayers];
            if (applySplatmap)
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        foreach (Site<byte, Vector2> s in v.GetSites())
                        {
                            if (v.GetDataAt(x, y) == s.GetID())
                            {
                                splatmapData[x, y, 0] = s.GetPosition().x / v.Width;
                                splatmapData[x, y, 1] = s.GetPosition().y / v.Height;
                                splatmapData[x, y, 2] = s.GetID() / (float)v.GetSites().Count;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = 1;
                        splatmapData[x, y, 1] = 1;
                        splatmapData[x, y, 2] = 1;
                    }
                }
            }
            long sEndTime = (DateTime.UtcNow.Ticks - sStartTime);

            //Apply data
            this.GetComponent<Terrain>().terrainData.SetAlphamaps(0, 0, splatmapData);
            this.GetComponent<Terrain>().terrainData.SetHeights(0, 0, v.GetFloatData());

            //Debug info
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text = "Size: " + voronoiSize + "^2\t\tVoronoi Diagram: " + TimeSpan.FromTicks(vEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Sites: " + numberOfSites + "\t\t\tSplatmapping: " + TimeSpan.FromTicks(sEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Seed: " + seed;

            PlayerPrefs.SetInt("terrainGen", 0);
        }
    }
}