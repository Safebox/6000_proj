﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugScreenshot : MonoBehaviour
{
    public bool capture;
    public int captureStart;
    public string screenshotSuffix;

    void Update()
    {
        if (capture && (Time.fixedUnscaledTime >= captureStart && Time.fixedUnscaledTime <= captureStart + 0.1f))
        {
            ScreenCapture.CaptureScreenshot("Screenshot_" + screenshotSuffix + ".png");
            Debug.Log("Screenshot_" + screenshotSuffix + " Done!");
        }
    }
}
