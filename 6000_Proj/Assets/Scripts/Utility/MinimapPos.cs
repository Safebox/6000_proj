﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapPos : MonoBehaviour
{
    public GameObject p;
    public Terrain dc;

	void Update ()
    {
        this.transform.position = new Vector3(p.transform.position.x, this.transform.position.y, p.transform.position.z);
        GameObject.Find("PosLabel").GetComponent<UnityEngine.UI.Text>().text = "Position: " + new Vector3Int((int)this.transform.position.x, (int)this.transform.position.y, (int)this.transform.position.z).ToString() + "\n";
        GameObject.Find("PosLabel").GetComponent<UnityEngine.UI.Text>().text += "Voronoi Cell ID: " + dc.terrainData.GetAlphamaps((int)this.transform.position.x, (int)this.transform.position.z, 1, 1)[0, 0, 2] * 255f;
    }
}
