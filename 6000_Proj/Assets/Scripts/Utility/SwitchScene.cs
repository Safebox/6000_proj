﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScene : MonoBehaviour
{
    void Start()
    {
        PlayerPrefs.SetInt("terrainSeed", 0);
        PlayerPrefs.SetInt("terrainGen", 0);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex - 1 > -1)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex - 1, UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - 2, UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1 < UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - 1)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1, UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
            }
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
                UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings - 1, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            PlayerPrefs.SetInt("terrainSeed", Random.Range(byte.MinValue, byte.MaxValue));
            PlayerPrefs.SetInt("terrainGen", 1);

            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
        else if (Input.GetKey(KeyCode.R))
        {
            PlayerPrefs.SetInt("terrainSeed", 0);
            PlayerPrefs.SetInt("terrainGen", 1);

            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }
}
