﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCaseGaussianBlur : MonoBehaviour
{
    public int terrainSize;
    private float[,,] splatmapData;
    private float[,] heightData;
    public bool applySplatmap;

    void Start()
    {
        if (PlayerPrefs.GetInt("terrainGen") == 1)
        {
            //Resize terrain as necessary
            this.GetComponent<Terrain>().terrainData.heightmapResolution = terrainSize + 1;
            this.GetComponent<Terrain>().terrainData.size = new Vector3(500, 255, 500);
            this.GetComponent<Terrain>().terrainData.alphamapResolution = terrainSize;

            //Setup Gaussian Blur
            long gStartTime = DateTime.UtcNow.Ticks;
            heightData = new float[terrainSize, terrainSize];
            for (int x = 0; x < heightData.GetLength(0); x++)
            {
                for (int y = 0; y < heightData.GetLength(1); y++)
                {
                    heightData[x, y] = GaussianFunction(x, y);
                }
            }
            long gEndTime = (DateTime.UtcNow.Ticks - gStartTime);

            //Setup splatmaps
            long sStartTime = DateTime.UtcNow.Ticks;
            splatmapData = new float[this.GetComponent<Terrain>().terrainData.alphamapWidth, this.GetComponent<Terrain>().terrainData.alphamapHeight, this.GetComponent<Terrain>().terrainData.alphamapLayers];
            if (applySplatmap)
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = heightData[x, y];
                        splatmapData[x, y, 1] = heightData[x, y];
                        splatmapData[x, y, 2] = heightData[x, y];
                    }
                }
            }
            else
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = 1;
                        splatmapData[x, y, 1] = 1;
                        splatmapData[x, y, 2] = 1;
                    }
                }
            }
            long sEndTime = (DateTime.UtcNow.Ticks - sStartTime);

            //Apply data
            this.GetComponent<Terrain>().terrainData.SetAlphamaps(0, 0, splatmapData);
            this.GetComponent<Terrain>().terrainData.SetHeights(0, 0, heightData);

            //Debug info
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text = "Size: " + terrainSize + "\t\tGaussian Blur: " + TimeSpan.FromTicks(gEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Splatmapping: " + TimeSpan.FromTicks(sEndTime).ToString() + "\n";

            PlayerPrefs.SetInt("terrainGen", 0);
        }
    }

    public float GaussianFunction(int x, int y)
    {
        float bellHeight = 1f;
        float bellCenterOffset = terrainSize / 2f;
        float bellWidth = bellCenterOffset / 4f;

        float gaussX = bellHeight * Mathf.Exp(-((Mathf.Pow(x - bellCenterOffset, 2f)) / (2f * Mathf.Pow(bellWidth, 2f))));
        float gaussY = bellHeight * Mathf.Exp(-((Mathf.Pow(y - bellCenterOffset, 2f)) / (2f * Mathf.Pow(bellWidth, 2f))));

        return Mathf.Sqrt(gaussX * gaussY);
    }
}
