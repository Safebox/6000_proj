﻿using System.Collections.Generic;
using UnityEngine;

public struct Voronoi
{
    //-----Data-----
    int vdWidth;
    int vdHeight;
    byte[,] vdData;

    List<Site<byte, Vector2>> vdSites;
    //--------------

    //-----Constructors-----
    public Voronoi(int width, int height)
    {
        vdWidth = width;
        vdHeight = height;
        vdData = new byte[vdWidth, vdHeight];

        vdSites = new List<Site<byte, Vector2>>();
    }
    //----------------------

    //-----Site Placement-----
    public void PlaceSite(byte id, int x, int y)
    {
        PlaceSite(id, new Vector2(x, y));
    }

    public void PlaceSite(byte id, Vector2 pos)
    {
        vdSites.Add(new Site<byte, Vector2>(id, pos));
    }

    public void PlaceSitesAtRandom(int n, int seed)
    {
        System.Random r = new System.Random(seed);

        for (byte s = 1; s < ((n < byte.MaxValue) ? n + 1 : byte.MaxValue); s++)
        {
            vdSites.Add(new Site<byte, Vector2>(s, new Vector2(r.Next(vdWidth - 1), r.Next(vdHeight - 1))));
        }
    }
    //------------------------

    //-----Region Generation-----
    public void GenerateEuclidean()
    {
        for (int x = 0; x < vdData.GetLength(0); x++)
        {
            for (int y = 0; y < vdData.GetLength(1); y++)
            {
                Vector2 newMin = Vector2.positiveInfinity;
                for (int i = 0; i < vdSites.Count; i++)
                {
                    if (Vector2.Distance(new Vector2(x, y), newMin) > Vector2.Distance(new Vector2(x, y), vdSites[i].GetPosition()))
                    {
                        newMin = vdSites[i].GetPosition();
                    }
                }
                for (int j = 0; j < vdSites.Count; j++)
                {
                    if (vdSites[j].GetPosition() == newMin)
                    {
                        SetDataAt(vdSites[j].GetID(), new Vector2(x,y));
                    }
                }
            }
        }
    }

    /*public void GenerateManhattan()
    {
        DateTime startTime = DateTime.UtcNow;
        //Store initial parsed positions
        List<Vector2> tempParsedPositions = new List<Vector2>();
        for (byte s = 1; s < vdSites.Count; s++)
        {
            tempParsedPositions.Add(vdSites[s]);
        }

        //Parse neighbours
        for (int i = 0; i < tempParsedPositions.Count; i++)
        {
            for (int x = -1; x < 1; x++)
            {
                for (int y = -1; y < 1; y++)
                {
                    //Check if value is within range
                    Vector2 offsetPositions = new Vector2((int)tempParsedPositions[i].x + x, (int)tempParsedPositions[i].y + y);
                    if (offsetPositions.x > 0 && offsetPositions.x < vdData.GetLength(0) && offsetPositions.y > 0 && offsetPositions.y < vdData.GetLength(1))
                    {
                        if (vdData[(int)offsetPositions.x, (int)offsetPositions.y] == 0)
                        {
                            //Fill neighbour is unoccupied and add to list
                            vdData[(int)offsetPositions.x, (int)offsetPositions.y] = vdData[(int)tempParsedPositions[i].x, (int)tempParsedPositions[i].y];
                            tempParsedPositions.Add(offsetPositions);
                        }
                    }
                }
            }
        }
        Debug.Log((DateTime.UtcNow - startTime).ToString());
    }*/
    //---------------------------

    //-----Utility-----
    public int Width
    {
        get
        {
            return vdWidth;
        }
    }

    public int Height
    {
        get
        {
            return vdHeight;
        }
    }

    public Site<byte,Vector2> GetSite(int index)
    {
        return vdSites[index];
    }

    public List<Site<byte, Vector2>> GetSites()
    {
        return vdSites;
    }

    private void SetDataAt(byte id, int x, int y)
    {
        SetDataAt(id, new Vector2(x, y));
    }

    private void SetDataAt(byte id, Vector2 pos)
    {
        vdData[(int)pos.x, (int)pos.y] = id;
    }

    public byte GetDataAt(int x, int y)
    {
        return GetDataAt(new Vector2(x, y));
    }

    public byte GetDataAt(Vector2 pos)
    {
        return vdData[(int)pos.x, (int)pos.y];
    }

    public byte[,] GetData()
    {
        return vdData;
    }

    public float[,] GetFloatData()
    {
        float[,] outputData = new float[vdData.GetLength(0), vdData.GetLength(1)];
        for (int x = 0; x < outputData.GetLength(0); x++)
        {
            for (int y = 0; y < outputData.GetLength(1); y++)
            {
                outputData[x, y] = vdData[x,y] / 255f;
            }
        }
        return outputData;
    }

    public void GetDebugData()
    {
        string o = "";
        for (int x = 0; x < vdData.GetLength(0); x++)
        {
            for (int y = 0; y < vdData.GetLength(1); y++)
            {
                o += vdData[x,y].ToString().PadLeft(3,'0') + ", ";
            }
            o += "\n";
        }
        Debug.Log(o);
    }
    //-----------------
}