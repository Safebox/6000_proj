﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCaseOpenSimplexNoise : MonoBehaviour
{
    public int coordinateMultiplyer;
    public int terrainSize;
    public int seed;
    private float[,,] splatmapData;
    private float[,] heightData;
    public bool applySplatmap;

    void Start()
    {
        if (PlayerPrefs.GetInt("terrainGen") == 1)
        {
            seed = PlayerPrefs.GetInt("terrainSeed");

            //Resize terrain as necessary
            this.GetComponent<Terrain>().terrainData.heightmapResolution = terrainSize + 1;
            this.GetComponent<Terrain>().terrainData.size = new Vector3(500, 255, 500);
            this.GetComponent<Terrain>().terrainData.alphamapResolution = terrainSize;

            //Setup OpenSimplexNoise
            OpenSimplexNoise osn = new OpenSimplexNoise(seed);
            long nStartTime = DateTime.UtcNow.Ticks;
            heightData = new float[terrainSize, terrainSize];
            for (int x = 0; x < heightData.GetLength(0); x++)
            {
                for (int y = 0; y < heightData.GetLength(1); y++)
                {
                    heightData[x, y] = (float)(osn.eval(x / (float)terrainSize * coordinateMultiplyer, y / (float)terrainSize * coordinateMultiplyer) + 1) / 2f;
                }
            }
            long nEndTime = (DateTime.UtcNow.Ticks - nStartTime);

            //Setup splatmaps
            long sStartTime = DateTime.UtcNow.Ticks;
            splatmapData = new float[this.GetComponent<Terrain>().terrainData.alphamapWidth, this.GetComponent<Terrain>().terrainData.alphamapHeight, this.GetComponent<Terrain>().terrainData.alphamapLayers];
            if (applySplatmap)
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = heightData[x, y];
                        splatmapData[x, y, 1] = heightData[x, y];
                        splatmapData[x, y, 2] = heightData[x, y];
                    }
                }
            }
            else
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = 1;
                        splatmapData[x, y, 1] = 1;
                        splatmapData[x, y, 2] = 1;
                    }
                }
            }
            long sEndTime = (DateTime.UtcNow.Ticks - sStartTime);

            //Apply data
            this.GetComponent<Terrain>().terrainData.SetAlphamaps(0, 0, splatmapData);
            this.GetComponent<Terrain>().terrainData.SetHeights(0, 0, heightData);

            //Debug info
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text = "Size: " + terrainSize + "\t\tOpenSimplex Noise: " + TimeSpan.FromTicks(nEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Seed: " + seed + "\t\t\tSplatmapping: " + TimeSpan.FromTicks(sEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Noise Coordinate Multiplyer: " + coordinateMultiplyer;

            PlayerPrefs.SetInt("terrainGen", 0);
        }
    }
}