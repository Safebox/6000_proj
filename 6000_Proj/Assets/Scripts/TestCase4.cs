﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCase4 : MonoBehaviour
{
    public int coordinateMultiplyer;
    public int scaleDivider;
    public int kernelSize;
    public int numberOfSites;
    public int voronoiSize;
    public int seed;
    private float[,,] splatmapData;
    private float[,] heightData;
    public bool applySplatmap;

    void Start()
    {
        if (PlayerPrefs.GetInt("terrainGen") == 1)
        {
            seed = PlayerPrefs.GetInt("terrainSeed");

            //Resize terrain as necessary
            this.GetComponent<Terrain>().terrainData.heightmapResolution = voronoiSize + 1;
            this.GetComponent<Terrain>().terrainData.size = new Vector3(500, 255f / scaleDivider, 500);
            this.GetComponent<Terrain>().terrainData.alphamapResolution = voronoiSize;

            //Setup Voronoi diagram
            Voronoi v = new Voronoi(voronoiSize, voronoiSize);
            long vStartTime = DateTime.UtcNow.Ticks;
            v.PlaceSitesAtRandom(numberOfSites, seed);
            long vEndTime = (DateTime.UtcNow.Ticks - vStartTime);

            //Setup splatmaps
            long sStartTime = DateTime.UtcNow.Ticks;
            splatmapData = new float[this.GetComponent<Terrain>().terrainData.alphamapWidth, this.GetComponent<Terrain>().terrainData.alphamapHeight, this.GetComponent<Terrain>().terrainData.alphamapLayers];
            if (applySplatmap)
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        Vector2 newMin = Vector2.positiveInfinity;
                        for (int i = 0; i < v.GetSites().Count; i++)
                        {
                            if (Vector2.Distance(new Vector2(x, y), newMin) > Vector2.Distance(new Vector2(x, y), v.GetSite(i).GetPosition()))
                            {
                                newMin = v.GetSite(i).GetPosition();
                            }
                        }
                        for (int j = 0; j < v.GetSites().Count; j++)
                        {
                            if (v.GetSite(j).GetPosition() == newMin)
                            {
                                splatmapData[x, y, 0] = v.GetSite(j).GetPosition().x / v.Width;
                                splatmapData[x, y, 1] = v.GetSite(j).GetPosition().y / v.Height;
                                splatmapData[x, y, 2] = v.GetSite(j).GetID() / (float)v.GetSites().Count;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int x = 0; x < splatmapData.GetLength(0); x++)
                {
                    for (int y = 0; y < splatmapData.GetLength(1); y++)
                    {
                        splatmapData[x, y, 0] = 1;
                        splatmapData[x, y, 1] = 1;
                        splatmapData[x, y, 2] = 1;
                    }
                }
            }
            long sEndTime = (DateTime.UtcNow.Ticks - sStartTime);

            //Setup OpenSimplexNoise
            OpenSimplexNoise osn;
            long nStartTime = DateTime.UtcNow.Ticks;
            long eEndTime = 0;
            heightData = new float[voronoiSize, voronoiSize];

            for (int x = 0; x < heightData.GetLength(0); x++)
            {
                for (int y = 0; y < heightData.GetLength(1); y++)
                {
                    //Setup edge transition
                    long eStartTime = DateTime.UtcNow.Ticks;
                    int edgeCount = 0;
                    float edgeValues = 0;
                    float edgeNormaliser = 0;

                    for (int i = -Mathf.FloorToInt(kernelSize * 0.5f); i < Mathf.FloorToInt(kernelSize * 0.5f) + 1; i++)
                    {
                        for (int j = -Mathf.FloorToInt(kernelSize * 0.5f); j < Mathf.FloorToInt(kernelSize * 0.5f) + 1; j++)
                        {
                            if ((x + i > -1 && y + j > -1) && (x + i < splatmapData.GetLength(0) && y + j < splatmapData.GetLength(1)))
                            {
                                if (splatmapData[x + i, y + j, 2] != splatmapData[x, y, 2])
                                {
                                    edgeCount++;
                                }
                                osn = new OpenSimplexNoise(seed + (int)(splatmapData[x + i, y + j, 2] * 255f));
                                edgeValues += GaussianFunction(i + Mathf.FloorToInt(kernelSize * 0.5f), j + Mathf.FloorToInt(kernelSize * 0.5f)) * (float)osn.eval(x / (float)voronoiSize * coordinateMultiplyer, y / (float)voronoiSize * coordinateMultiplyer);
                                edgeNormaliser += GaussianFunction(i + Mathf.FloorToInt(kernelSize * 0.5f), j + Mathf.FloorToInt(kernelSize * 0.5f));
                            }
                        }
                    }
                    float edgeStrength;
                    if (edgeCount > 0)
                    {
                        edgeStrength = edgeValues / edgeNormaliser;
                    }
                    else
                    {
                        osn = new OpenSimplexNoise(seed + (int)(splatmapData[x, y, 2] * 255f));
                        edgeStrength = (float)osn.eval(x / (float)voronoiSize * coordinateMultiplyer, y / (float)voronoiSize * coordinateMultiplyer);
                    }
                    heightData[x, y] = (edgeStrength + 1) / 2;
                    eEndTime += (DateTime.UtcNow.Ticks - eStartTime);
                    //-----
                }
            }
            long nEndTime = (DateTime.UtcNow.Ticks - nStartTime - eEndTime);

            //Apply data
            this.GetComponent<Terrain>().terrainData.SetAlphamaps(0, 0, splatmapData);
            this.GetComponent<Terrain>().terrainData.SetHeights(0, 0, heightData);

            //Debug info
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text = "Size: " + voronoiSize + "^2\t\tVoronoi Diagram (w/o Regions): " + TimeSpan.FromTicks(vEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Sites: " + numberOfSites + "\t\t\t\tOpenSimplex Noise (w/ regions): " + TimeSpan.FromTicks(nEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Seed: " + seed + "\t\t\t\tSplatmapping (w/ Sites): " + TimeSpan.FromTicks(sEndTime).ToString() + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Noise Coordinate Multiplyer: " + coordinateMultiplyer + "\n";
            GameObject.Find("DetailsLabel").GetComponent<UnityEngine.UI.Text>().text += "Edge Transition " + kernelSize + "^2 Kernel (Total): " + TimeSpan.FromTicks(eEndTime).ToString() + "\n";

            PlayerPrefs.SetInt("terrainGen", 0);
        }
    }

    public float GaussianFunction(int x, int y)
    {
        float bellHeight = 1f;
        float bellCenterOffset = kernelSize;
        float bellWidth = bellCenterOffset / 4f;

        float gaussX = bellHeight * Mathf.Exp(-((Mathf.Pow(x - bellCenterOffset, 2f)) / (2f * Mathf.Pow(bellWidth, 2f))));
        float gaussY = bellHeight * Mathf.Exp(-((Mathf.Pow(y - bellCenterOffset, 2f)) / (2f * Mathf.Pow(bellWidth, 2f))));

        return Mathf.Sqrt(gaussX * gaussY);
    }
}
