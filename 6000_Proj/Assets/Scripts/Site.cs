﻿public struct Site<A, B>
{
    //-----Data-----
    A _id;
    B _position;
    //--------------

    //-----Constructors-----
    public Site(A id, B position)
    {
        _id = id;
        _position = position;
    }
    //----------------------

    //-----Utility-----
    public B GetPosition()
    {
        return _position;
    }

    public A GetID()
    {
        return _id;
    }
    //-----------------
}